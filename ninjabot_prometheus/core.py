import datetime
import json
import random
import time
import re
import sys
import threading
import dateutil.parser

from flask import Flask, request


app = Flask(__name__)

@app.route('/', methods=['POST'])
def foo():
    try:
        notif = request.data.decode('utf-8')
    except Exception:
        notif = request.data
    print('Received:', notif, file=sys.stderr)
    data = json.loads(notif)
    push_update = app.config['push_update']
    colors = app.config["colors"]
    for alert in data.get('alerts', []):
        # Retrieve metadata
        status = alert.get('status', 'firing')
        labels = alert.get('labels', {})
        starts_at = alert.get('startsAt', '')
        if starts_at:
            starts_at = dateutil.parser.isoparse(starts_at)
            duration = datetime.datetime.now(starts_at.tzinfo) - starts_at
            if duration < datetime.timedelta(minutes=1):
                duration = ''
            elif duration < datetime.timedelta(hours=1):
                duration = 'for {} minutes !'.format(
                    int(duration.total_seconds()//60))
            elif duration < datetime.timedelta(days=1):
                duration = 'for {} hours {} minutes !'.format(
                    int(duration.total_seconds()//3600), int((duration.total_seconds()//60) % 60))
            elif duration < datetime.timedelta(days=30):
                duration = 'for {} days !'.format(
                    int(duration.total_seconds()//86400))
            elif datetime.date.today().weekday() == 0:
                duration = 'for … waaay too long ({})'.format(duration)
            else:
                continue
        else:
            duration = ''
        instance = labels.get('instance', '')
        alertname = labels.get('alertname', '')
        severity = labels.get('severity', 'unknown')
        annotations = alert.get('annotations', {})
        summary = annotations.get('summary', '')

        if status == 'resolved':
            # Send a discret resolved message
            # See https://modern.ircdocs.horse/formatting.html#colors-16-98
            message = f'\x0310{alertname} \x0303resolved\x0315 {summary}\x0F'
        else:
            # Send an alert
            color = colors.get(severity, "\x0354")
            message = f'\x0310{alertname} {color}{severity}\x0F {summary} {duration}'
        print(f'Pushing update: {alertname} {summary}', file=sys.stderr)
        push_update(instance, message)

    return "OK"


class Prometheus:
    def __init__(self, fifo, address, port, colors, name="prometheus"):
        self.fifo = fifo
        self.address = address
        self.port = port
        self.colors = colors
        self.name = name


    def push_update(self, page, message):
        time.sleep(random.random()*3)
        with open(self.fifo, 'w') as fifo:
            fifo.write(
                json.dumps(
                    {'source': self.name, 'page': page, 'message': message}, 
                    separators=(',', ':')
                )+"\n")

    def loop(self, *args, **kwargs):
        app.config['push_update'] = self.push_update
        app.config["colors"] = self.colors
        app.run(host=self.address, port=self.port)
    
    def run(self):
        self.thread = threading.Thread(target=self.loop)
        self.thread.start()
