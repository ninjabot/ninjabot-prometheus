import argparse

import yaml

from ninjabot_prometheus.core import Prometheus


# /!\ By defaut tmux does not show 8-bits color!
# default color levels for prometheus alerts
COLOR_LEVEL = {
    "critical": "\x034",
    "warning": "\x038",
}

def cli():
    parser = argparse.ArgumentParser("ninjabot-prometheus", description="Prometheus module for ninjabot")
    parser.add_argument("--config", type=str, required=False, default="ninjabot.conf")
    parser.add_argument("--name", type=str, required=False)
    args = parser.parse_args()

    config = yaml.load(open(args.config), Loader=yaml.BaseLoader)
    if args.name:
        prometheus_config = config[f"prometheus.{args.name}"]
    else:
        prometheus_config = config["prometheus"]

    listener = Prometheus(
        config["core"]["fifo"],
        prometheus_config.get("address", "127.0.0.1"), 
        prometheus_config.get("port", 5000),
        prometheus_config.get("colors", COLOR_LEVEL),
        name=prometheus_config.get("name") or args.name or "prometheus"
    )
    listener.run()