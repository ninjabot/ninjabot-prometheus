# How to
The systemd service available in the repository expect you to install ninjabot-prometheus in a virtualenv in `/opt/ninjabot/env`,
and to have a `ninjabot` user and group on your system.

You can do it with the following on a debian system:
```
sudo adduser --system --home /opt/ninjabot ninjabot
sudo -u ninjabot python3 -m venv /opt/ninjabot/env
```
And then to really install ninjabot:
```
sudo -u ninjabot /opt/ninjabot/env/pip install git+https://gitlab.crans.org/ninja/ninjabot-prometheus.git
sudo -u ninjabot cp /opt/ninjabot/env/lib/python<your version>/site-packages/ninjabot_prometheus/ninjabot.conf.example /opt/ninjabot/ninjabot.conf
sudo cp /opt/ninjabot/env/lib/python<your version>/site-packages/ninjabot_prometheus/ninjabot-prometheus.service /etc/systemd/system/ninjabot-prometheus.service
```
You can edit the config file you've copied in `/opt/ninjabot/ninjabot.conf` (documentation below).
And the start and enable the systemd service.
```
sudo systemctl start ninjabot.service
sudo systemctl enable ninjabot.service
```
If you want to deploy multiple instances of `ninjabot-prometheus`, you can use the following systemd service template `/opt/ninjabot/env/lib/python<your version>/site-packages/ninjabot_prometheus/ninjabot-prometheus@.service`.

# Configuration
```
core:
  nick: <bot nickname>
  server: <server ip>
  port: <server port>
  account: <NickServ acount name>
  password: <NickServ account password>
  channels: <list of chans that will receive notifications>
    - <chan1>
    - <chan2>
  # you can also define regex for each channel with:
  #  - [<chan3>, <regex on the provider name>]
  fifo: <path of the fifo, by default: "/tmp/ninjabot.fifo">
  tls: <enable tls>
  tls_verify: <verify server certificate>
  logfile: <path to logfile>
  verbose: <enable debug logs>
  client_cert: <path to the client certificate>
  client_key: <path to the client certificate private key>
# if you want to use only one instance
prometheus:
  address: <ip on which the webhook server will listen>
  port: <port on wich the webhook will listen>
  colors: <dict of alert type + colors cf: https://modern.ircdocs.horse/formatting.html#colors>
    <alter type>: "\x03<color code>"
    critical: "\x0304"
    warning: "\x0308"
  name: <the name of the provider, used by the core regexes for channels, by default: prometheus>
# if you want multiple instances:
prometheus.<instance name>:
  address: <ip on which the webhook server will listen>
  port: <port on wich the webhook will listen>
  colors: <dict of alert type + colors cf: https://modern.ircdocs.horse/formatting.html#colors>
    <alter type>: "\x03<color code>"
    critical: "\x0304"
    warning: "\x0308"
  name: <the name of the provider, used by the core regexes for channels, by default: instance name>
```